#!/bin/bash

# ======================================================================================
# ======================================================================================
#                                 'curl' doit être installé
#
#                 Lancement de ce script sur la VM en lançant la commande
#
#  curl -sL https://gitlab.com/jeromelegendre/pfsinfra-public/-/raw/master/loader.sh -o loader.sh && bash loader.sh && rm -f loader.sh
#
# ======================================================================================
# ======================================================================================

# Definitions
[ -z $PATH_APPS ]         && PATH_APPS="/app"     # emplacement des depots
[ -z $DATAS ]             && DATAS="${PATH_APPS}/__data__"         # datas du projet (hors fichiers de donees backend)
[ -z $GRP_APP ]           && GRP_APP="pfsapp"       # groupe systeme ayant les droits d'ecriture sur la hierarchie
[ -z $GIT_BRANCH_INFRA ]  && GIT_BRANCH_INFRA="FEATURE_segmentation_tokens"

PFSINFRAPATH="${PATH_APPS}/pfsinfra"
CURRENT_USER=$(id -n -u)
DEPOT_INFRA="git@gitlab.com:jeromelegendre/pfsinfra.git"
SSH_REPO_KEY="${HOME}/.ssh/id_rsa_pfs"
GIT_CLONE="GIT_SSH_COMMAND='ssh -i "
GIT_CLONE+=${SSH_REPO_KEY}
GIT_CLONE+="' git clone"

sudo="sudo"
[ "$CURRENT_USER" == "root" ] && sudo=""




# Detection OS
declare -A osInfo;
osInfo[/etc/redhat-release]=redhat
osInfo[/etc/debian_version]=debian
for f in "${!osInfo[@]}"
do
    if [ -f $f ];then
        os=${osInfo[$f]}
        case $os in
          redhat)
            package_update="yum update -y -q -e 1"
            package_install="yum install -y"
            package_remove="yum remove -y"
            ;;
          debian)
            package_update="apt-cache update -y -qq"
            package_install="apt-get install -y"
            package_remove="apt-get remove"
            ;;
        esac
    fi
done

if [ -z ${os} ];then
  echo "Le système d'exploitation de cette machine n'est pas compatible avec ce script. Terminé"
  exit
fi
echo "Système d'exploitation ${os}"


# Compte avec pouvoir ?
if [ "${CURRENT_USER}" != "root" ] && [ -z "$( groups | grep 'wheel' )" ]; then
  echo "Votre compte ne possède pas de pouvoir d'administration. Terminé"
  exit
fi
echo "Votre compte possède des pouvoir d'administration"






# Installations prealables
echo "Mise à jour de la base des packages"
$package_update
case $os in
  redhat)
    $sudo $package_install git mysql yum-utils bzip2
    ;;
  debian)
    $sudo $package_install git mysql-client bzip2
    ;;
esac





# Generation d'une cle ssh projet si inexistante
echo "Test de présence de la clé SSH PFS"
if [ ! -f $SSH_REPO_KEY ]; then
  read -p "Vous ne semblez pas posséder de clé SSH RSA PFS, souhaitez-vous que j'en créé une ? (oui|non)(Defaut: oui): " create_ssh_key
  create_ssh_key="$(echo "$create_ssh_key" | awk '{print tolower($0)}')"
  if [ "${create_ssh_key}" != 'non' ];then
    ssh-keygen -q -t rsa -N '' -f $SSH_REPO_KEY <<< ""$'\n'"y" 2>&1 >/dev/null
    chmod 600 $SSH_REPO_KEY
    echo "=========================================================================="
    echo
    echo "Veuillez coller cette clé publique dans votre compte https://gitlab.com."
    echo "Faites approuver ce compte par le projet PFS, puis relancez ce script."
    echo
    cat "${SSH_REPO_KEY}.pub"
    echo
    echo "=========================================================================="
    exit
  fi
fi

# Ajoute le repo dans les hotes ssh connus
ssh-keyscan -H "gitlab.com" >> ~/.ssh/known_hosts

# Confirmation de la validation du compte gitlab
echo "----------> Clef publique SSH PFS"
cat ${SSH_REPO_KEY}.pub
echo "----------"
WARNING="
la clé SSH publique de ce compte doit être inscrite dans un compte gitlab.com approuvé par l'équipe Atos. Saisissez 'ok' si c'est le cas. Sinon, une fois fait, relancez ce script"
read -p "${WARNING} : " validate
[  "$(echo "$validate" | awk '{print tolower($0)}')" != "ok" ] && exit

# demarre l'agent SSH et y ajoute la cle PFS
eval "$(ssh-agent -s)"
ssh-add $SSH_REPO_KEY





# Installer Docker ?
read -p 'Installer docker ? (oui|non)(Defaut: oui): ' install_docker
[ -z $install_docker ] && install_docker="oui"
if [ "$(echo "$install_docker" | awk '{print tolower($0)}')" == "oui" ]; then
  DOCKERCOMPOSE="/usr/local/bin/docker-compose"

  # preparation
  case $os in
    redhat)
      $sudo $package_remove docker \
                      docker-client \
                      docker-client-latest \
                      docker-common \
                      docker-latest \
                      docker-latest-logrotate \
                      docker-logrotate \
                      docker-engine \
                      || exit
      $sudo yum-config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo || exit
      ;;
    debian)
      $sudo $package_install  apt-transport-https \
                    ca-certificates \
                    gnupg-agent \
                    software-properties-common \
                    || exit
      $sudo curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -  || exit
      $sudo add-apt-repository \
         "deb [arch=amd64] https://download.docker.com/linux/debian \
         $(lsb_release -cs) \
         stable" \
         || exit
      ;;
  esac

  # Installation Docker
  echo "Installation de Docker"
  $sudo $package_update || exit
  $sudo $package_install  docker-ce docker-ce-cli containerd.io || exit
  $sudo docker --version || exit

  # Installation docker-compose
  echo "Installation de docker-compose"
  $sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o ${DOCKERCOMPOSE} || exit
  $sudo chmod +x /usr/local/bin/docker-compose
  $sudo ${DOCKERCOMPOSE} --version || exit

  # Demarrage automatique Docker
  echo "Démarrage automatique de docker"
  $sudo systemctl enable docker
  $sudo systemctl start docker
fi





# Creer le groupe applicatif
egrep -i "^${GRP_APP}" /etc/group;
if [ $? -ne 0 ]; then
  echo "..... creation du groupe applicatif"
  $sudo groupadd -f ${GRP_APP} || (
      echo "Impossible de creer le groupe applicatif ${GRP_APP}."
      exit
    )
else
  echo "Le groupe applicatif existe"
fi

# Ajout de l'utilisateur courant au groupe applicatif
egrep -i "^${CURRENT_USER}:" /etc/passwd;
if [ $? -ne 0 ]; then
  echo "..... ajout de l'utilisateur courant au groupe applicatif"
  $sudo usermod -aG ${GRP_APP} ${CURRENT_USER} || (
      echo "Impossible d'ajouter ${CURRENT_USER} au groupe applicatif ${GRP_APP}."
      exit
    )
else
  echo "Le compte ${CURRENT_USER} appartient bien au groupe applicatif ${GRP_APP}."
fi

# Cree le dossier applicatif
echo "Creation du dossier applicatif ${PATH_APPS}"
$sudo mkdir -p ${PATH_APPS} || exit
$sudo chown ${CURRENT_USER} ${PATH_APPS}  || exit

# Clone le depot d'infrastructure docker
echo "..... clonage du depot d'infrastructure Docker"
echo $GIT_CLONE
cd ${PATH_APPS}
if ! ssh-agent bash -c "ssh-add $SSH_REPO_KEY; git clone --branch $GIT_BRANCH_INFRA $DEPOT_INFRA" ; then
    echo "OOO-- Impossible de cloner le dépôt GIT ${DEPOT_INFRA}, avez-vous inséré votre clé publique sur le compte gitlab, ce compte est-il approuvé par le projet ? (voir manuel d'installation) --OOO"
    exit
fi

# Proprietaire sur la hierarchie applicative
echo "..... changement de proprietaire sur la hierarchie ${PFSINFRAPATH}"
if ! chown -R ${CURRENT_USER}:${GRP_APP} ${PFSINFRAPATH}; then
    echo "Impossible de changer de propriétaire"
    exit
fi

echo "=========================================================================="
echo "                              Terminé"
echo
echo "Le présent script doit être exécuté sur toutes les VMs de l'environnement."
echo
echo "   Une fois fait, executer le script de deploiement applicatif sur la"
echo "                             ** VM3 **"
echo
echo "           bash ${PFSINFRAPATH}/scripts/installation_app.sh"
echo
echo "=========================================================================="
